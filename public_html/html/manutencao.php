<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">   
    <!-- custom CSS -->
    <link rel="stylesheet" href="../css/estilo.css">    <!--Configuração do slogan -->
    <!-- <script type="text/javascript" src="public_html/js/jquery.min.js"></script>  -->
    <!-- <script type="text/javascript" src="public_html/js/custom.js"></script>  -->
    <link rel="stylesheet" href="">   

    <title>Manutenção</title>      
    <style>
        div.realocar{margin: 0 170px 0 250px;}       /* Correção da centralização das imagens */
        p.text-justify{text-indent: 2em;}    /* Paragráfo */
        div.subtitle{color: #FFFFFF;}
        li.dropdown{margin-right: 15px;}     /* Espaçamento entre <ul> <li> */
        li.nav-item:last-child{margin-left: 20px;}   /* Espaçamento somente na última <li> do navbar*/
        h5.textowappix1{color: black;}
        p.textowappix1{color: black;}
        div.imagencontainer{background-color: #e9e9e9;}
        .modal{margin: -29px 0 0 0;width: 500px;height: 700px;}
        .modal-content{width: 300px;height: 580px;}      
        nav.shadow{margin: 0px;padding: 0px;}     
        img.brasao{/* height: 75px; width: 75px; */} 
        ol, li{list-style: none;}
        
    </style>

  </head>
  <body>
    <!-- Barra de navegação MENU -->  <!-- Modal -->
    <nav class="navbar navbar-expand-sm navbar-expand-xs navbar-dark bg-dark fixed-top">    
        <div class="container">  
        <nav class="navbar navbar-dark bg-dark shadow">
          <span class="navbar-brand mb-0 h1"></span>
            <button class="navbar-toggler-icon" type="button" data-toggle="modal" data-target="#modalExemplo"></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </nav>

      <!-- nome do slogan -->   
        <h1><a class="navbar-brand" href="../../index.php">WAPPIX</a></h1>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
              <span class="navbar-toggler-icon"></span>           
          </button> 

        <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
          <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../../index.php">Home <span class="sr-only">(página atual)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="clientes.php">Clientes</a>
                </li>              
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Serviços
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">                      
                    <a class="dropdown-item" href="#">Suporte</a>
                    <div class="dropdown-divider"></div>                      
                      <a class="dropdown-item" href="#">Personalização</a>
                      <a class="dropdown-item" href="#">Cloud Computing</a>
                      <a class="dropdown-item" href="#">Consultoria e Implantação</a>
                    </div>
                </li>     
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Soluções
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">   
                      <a class="dropdown-item" href="#">Sistemas</a>                     
                      <a class="dropdown-item" href="#">Infraestrutura</a>
                      <a class="dropdown-item" href="#">Desenvolvimento</a>                        
                    <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Ambiente de Testes</a>
                      <a class="dropdown-item" href="#">Ambiente de Produção</a>
                      <a class="dropdown-item" href="#">Ambiente de Homologação</a>
                      <a class="dropdown-item" href="#">Ambiente de Desenvolvimento</a>
                    </div>
                </li>  
                <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Ajuda
                      </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">   
                      <a class="dropdown-item" href="#">Sobre</a>                        
                      <a class="dropdown-item" href="#">PT-BR</a>                      
                      <a class="dropdown-item" href="#">PT-BR</a>
                    <div class="dropdown-divider"></div>                     
                      <a class="dropdown-item" href="#">Contatos</a>  
                      <a class="dropdown-item" href="#">Dica Rápida</a>
                    </div>
                </li>   
                <li class="nav-item">
                <a href="form.php"><button type="button" class="btn btn-success my-2 my-sm-0" type="submit">Entrar</button></a>
                </li>             
          </ul>           
        </div>
</nav>
<!-- Como um link -->

 <nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand" href="#">LGPD</a>
 </nav>

<!-- Como um span (Segundo nav que some, linha da pesquisa)-->
<nav class="navbar navbar-dark bg-dark mt-3">
   <div class="container">  
        <div class="menu mt-3">       
              <div class="subtitle">O seu suporte on-line</div>
        </div>
        <div class="search">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
                <button type="button" class="btn btn-outline-primary my-2 my-sm-0" type="submit">Pesquisar</button>
            </form>
        </div>
<!-- Modal que foi acessado da linha 41 e até a 43 barra nav -->
    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">MENU</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                   <!-- Conteúdo do MENU -->
                   <ol>
                      <!-- <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(página atual)</span></a>
                      </li> -->
                      <li class="nav-item">
                          <a class="nav-link" href="empresa.php">Empresa</a>
                      </li>   
                      <li class="nav-item">
                          <a class="nav-link" href="clientes.php">Clientes</a>
                      </li>        
                      <li class="nav-item">
                          <a class="nav-link" href="teste.php">Diagnóstico de sites</a>
                      </li>          
                      <li class="nav-item">
                          <a class="nav-link" href="manutencao.php">Diagnóstico de sites</a>
                      </li>      


                  </ol>
            </div>
            <div class="modal-footer">
              <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
          </div>
        </div>
    </div>
  </div> 
</nav>

<!-- Conteúdo -->
<div class="container mt-4 mb-4">
  <div style="text-align: center;">
    <img src="../../arquivos/obra.png" alt="" style="text-align: center;">
  </div>

</div>

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Section: Social media -->
  <section class="container justify-content-center justify-content-lg-between p-4 border-bottom" >
    <!-- Left -->
    
    <div class="me-5 d-none d-lg-block">
      <p class="text-justify"><span>Lei Geral de Proteção de Dados Pessoais (LGPD).: Lei nº 13.709/2018, é a legislação brasileira que regula as atividades de tratamento de dados pessoais e que também altera os artigos 7º e 16 do Marco Civil da Internet.</span></p>
    </div>
   
    <!-- Left -->

    <!-- Right -->
            <!-- <div>
            <a href="" class="me-4 text-reset ">
                <i class="fab fa-facebook-f">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-twitter">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-google">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-instagram">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-linkedin">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-github">teste</i>
            </a>
            </div> -->
    <!-- Right -->
  </section>
  <!-- Section: Social media -->

  <!-- Section: Links  -->
  <section class="d-flex justify-content-center justify-content-lg-between p-2 border-bottom">
    <div class="container text-center text-md-start mt-1">
      <!-- Grid row -->
      <div class="row mt-1">
        <!-- Grid column -->
        <div class="col-sm-3 col-lg-4 col-xs-3 mx-auto mb-1">
          <!-- Content -->
          <h6 class="text-uppercase fw-bold mb-4">
            <i class="fas fa-gem me-3"></i>Segurança da Informação
          </h6>
          <p class="text-justify">
          A segurança da informação está diretamente relacionada com proteção de um conjunto de informações, no sentido de preservar o valor que possuem para um indivíduo ou uma organização. São propriedades básicas da segurança da informação: confidencialidade, integridade, disponibilidade, autenticidade e legalidade.
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Produtos
          </h6>        
          <p>
            <a href="#!" class="text-reset">Integridade </a>
          </p>   
          <p>
            <a href="#!" class="text-reset">Autenticidade </a>
          </p>   
          
          <p>
            <a href="#!" class="text-reset">Disponibilidade </a>
          </p>

          <p>
            <a href="#!" class="text-reset">Confidencialidade</a>
          </p>       
        </div>        

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            links Úteis
          </h6>
          <p>
            <a href="#!" class="text-reset">Ajuda</a>
          </p>        
          <p>
            <a href="#!" class="text-reset">Normas</a>
          </p> 
          <p>
            <a href="#!" class="text-reset">Instruções</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Configurações</a>
          </p>
        </div>
        
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Contato
          </h6>
          <h6 class="text-left"><i class="fas fa-home me-3">Península dos Ministros<br/>St. de Habitações Individuais Sul <br/>QL 12 - Lago Sul, Brasília - DF<br/> 70385-010</i></h6>
          <p>
            <i class="fas fa-envelope me-3"></i>
            Wappixbr@gmail.com
          </p>
          <p><i class="fas fa-phone me-3"></i> + 55 61 9.9267-7435</p>
          <p><i class="fas fa-print me-3"></i> + 55 61 9.9999-9999</p>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
  </section>
  <!-- Section: Links  -->

  <!-- Copyright -->
  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="https://www.google.com.br">Wappix.com</a>   <!--  https://mdbootstrap.com/  -->
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
      </div>

    <script src="../js/jquery-3.3.1.slim.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    
  </body>
</html>



