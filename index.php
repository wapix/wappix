<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <!-- Meta tags Obrigatórias -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="public_html/css/bootstrap.min.css">   
    <!-- custom CSS -->
    <link rel="stylesheet" href="public_html/css/estilo.css">    <!--Configuração do slogan -->
    <!-- <script type="text/javascript" src="public_html/js/jquery.min.js"></script>  -->
    <!-- <script type="text/javascript" src="public_html/js/custom.js"></script>  -->
    <link rel="stylesheet" href="">   

    <title>Wappix</title>      
    <style>
        div.realocar{margin: 0 170px 0 250px;}       /* Correção da centralização das imagens */
        p.text-justify{text-indent: 2em;}    /* Paragráfo */
        div.subtitle{color: #FFFFFF;}
        li.dropdown{margin-right: 15px;}     /* Espaçamento entre <ul> <li> */
        li.nav-item:last-child{margin-left: 20px;}   /* Espaçamento somente na última <li> do navbar*/
        h5.textowappix1{color: black;}
        p.textowappix1{color: black;}
        div.imagencontainer{background-color: #e9e9e9;}
        .modal{margin: -29px 0 0 0;width: 500px;height: 700px;}
        .modal-content{width: 300px;height: 580px;}      
        nav.shadow{margin: 0px;padding: 0px;}     
        img.brasao{/* height: 75px; width: 75px; */} 
        ol, li{list-style: none;}
     
    </style>

  </head>
  <body>
    <!-- Barra de navegação MENU -->   
<!-- <nav class="navbar navbar-expand-sm fixed-top navbar-dark bg-dark"> -->
<nav class="navbar navbar-expand-sm navbar-expand-xs navbar-dark bg-dark fixed-top">    
    <div class="container">
      <!-- chamando modal do botão MENU -->
    <nav class="navbar navbar-dark bg-dark shadow">
          <span class="navbar-brand mb-0 h1"></span>
            <button class="navbar-toggler-icon" type="button" data-toggle="modal" data-target="#modalExemplo"></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </nav>
     <a href="index.php"><img class="brasao" src="arquivos/sloganwappix.jpg"/></a>
        <!-- <h1><a class="navbar-brand" href="index.php">WAPPIX</a></h1> -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
            <ul class="navbar-nav ml-auto">
                <!-- <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(página atual)</span></a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link" href="public_html/html/empresa.php">Empresa</a>
                </li>       
                <!-- <li class="nav-item">
                    <a class="nav-link" href="public_html/html/clientes.php">Clientes</a>
                </li>            -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Serviços
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Suporte</a>
                    <div class="dropdown-divider"></div>                      
                      <a class="dropdown-item" href="#">Personalização</a>
                      <a class="dropdown-item" href="#">Cloud Computing</a>
                      <a class="dropdown-item" href="#">Consultoria e Implantação</a>
                    </div>
                </li>     
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Soluções
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">   
                      <a class="dropdown-item" href="#">Sistemas</a>                     
                      <a class="dropdown-item" href="#">Infraestrutura</a>
                      <a class="dropdown-item" href="#">Desenvolvimento</a>                        
                    <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Ambiente de Testes</a>
                      <a class="dropdown-item" href="#">Ambiente de Produção</a>
                      <a class="dropdown-item" href="#">Ambiente de Homologação</a>
                      <a class="dropdown-item" href="#">Ambiente de Desenvolvimento</a>
                    </div>
                </li>  
                <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Ajuda
                      </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">    
                      <a class="dropdown-item" href="#">Sobre</a>                       
                      <a class="dropdown-item" href="#">PT-BR</a>                      
                      <a class="dropdown-item" href="#">PT-BR</a>
                    <div class="dropdown-divider"></div>                     
                      <a class="dropdown-item" href="#">Contatos</a>  
                      <a class="dropdown-item" href="#">Dica Rápida</a>
                    </div>
                </li>   
                <li class="nav-item">
                  <a href="public_html/html/form.php"><button type="button" class="btn btn-success my-2 my-sm-0" type="submit">Entrar</button></a>
                </li>             
            </ul>           
        </div>
</nav>

 <!-- Como um link -->

 <nav class="navbar navbar-dark bg-dark">
  <a class="navbar-brand" href="#">LGPD</a>
</nav>

<!-- Como um span -->
<nav class="navbar navbar-dark bg-dark mt-3">
   <div class="container">
  <!-- <span class="navbar-brand mb-3 h2">LGPD</span>  -->
        <div class="menu mt-3">
              <!-- <button class="br-button" type="button" circle mini><i class="fas fa-bars"></i>
              </button> -->
              <!-- <div class="title"> -->
                <!-- <a class="" href="index.php">DRPS</a> -->
                <!-- <span class="navbar-brand mb-4 h2">O seu suporte on-line</span>                  -->
              <!-- </div> -->
              <div class="subtitle">O seu suporte on-line</div>
        </div>
        <div class="search">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" aria-label="Pesquisar">
                <button type="button" class="btn btn-outline-primary my-2 my-sm-0" type="submit">Pesquisar</button>
            </form>
        </div>

        <!-- Modal que foi acessado da linha 41 e até a 43 barra nav -->
    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">MENU</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body ml-auto col-xs-12 col-sm-12">
                <!-- Conteúdo do MENU -->
                  <ol>
                      <!-- <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(página atual)</span></a>
                      </li> -->
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/empresa.php">Empresa</a>
                      </li>    
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/clientes.php">Clientes</a>
                      </li>     
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/teste.php">Diagnóstico de sites</a>
                      </li>          
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/manutencao.php">Portal do Cliente</a>
                      </li>     
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/teste.php">Notícias</a>
                      </li>   
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/manutencao.php">Store</a>
                      </li>   
                      <li class="nav-item">
                          <a class="nav-link" href="public_html/html/manutencao.php">Download</a>
                      </li> 


                  </ol>
                

            </div>
            <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button> -->
            </div>
          </div>
        </div>
    </div>
  </div> 
</nav>
    </div> 
</nav>

<!-- conteúdo  -->


<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="arquivos/Wappixslogancarrossel.jpg" alt="Primeiro Slide"  height=" 451px;">
      <div class="carousel-caption d-none d-md-block">
        <!-- <h5 class="textowappix1">Empresa especializada na LGPD</h5> -->
        <p class="textowappix1">Tecnologia de Segurança da Informação</p>
      </div>

    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="arquivos/matrix.jpg" alt="Segundo Slide" height=" 451px;">
      <div class="carousel-caption d-none d-md-block">
        <h5>LGPD</h5>
        <p>A Lei Geral de Proteção de Dados Pessoais, Lei nº 13.709/2018, é a legislação brasileira que regula as atividades de tratamento de dados pessoais e que também altera os artigos 7º e 16 do Marco Civil da Internet.</p>
      </div>      
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="arquivos/cidade.jpg" alt="Terceiro Slide" height=" 451px;">
      <div class="carousel-caption d-none d-md-block">
        <h5>Sobre Wappix</h5>
        <p>Empresa especializada na LGPD</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="arquivos/quadrado.jpg" alt="Quarto Slide" height=" 451px;">
      <div class="carousel-caption d-none d-md-block">
        <h5>Sobre Wappix</h5>
        <p>Empresa especializada na LGPD</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="arquivos/imgtec.jpg" alt="Quinto Slide" height=" 451px;">
      <div class="carousel-caption d-none d-md-block">
        <h5>Sobre Wappix</h5>
        <p>Empresa especializada na LGPD</p>
      </div>
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>
<!-- Conteúdo do corpo da aplicação -->

<div class="container mt-0">
    <div class="row mt-0">
        <div class="col-sm-6">
            <div class="card">
              <div class="card-body">
                  <h5 class="card-title">Política (Nível estratégico) </h5>
                  <p class="card-text text-justify">Define as regras de alto nível que representam os princípios básicos para incorporar à sua gestão de acordo com a visão estratégica. Serve como base para que as normas e os procedimentos sejam criados e detalhados.</p>
                  <a href="#" class="btn btn-primary">Visitar</a>
              </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
              <div class="card-body">
                  <h5 class="card-title">Normas (Nível tático)</h5>
                  <p class="card-text text-justify">Especificam, no plano tático, as escolhas tecnológicas e os controles que deverão ser implementados para alcançar a estratégia definida nas diretrizes da política.</p><br/>
                  <a href="#" class="btn btn-primary">Visitar</a>
              </div>
            </div>
        </div>
        <div class="col-sm-6 mt-1">
            <div class="card">
              <div class="card-body">
                  <h5 class="card-title">Procedimentos (Nível operacional)</h5>
                  <p class="card-text text-justify">Instrumentalizam o disposto nas normas e na política, permitindo a direta aplicação nas atividade.</p><br/>
                  <a href="#" class="btn btn-primary">Visitar</a>
              </div>
            </div>
        </div>
        <div class="col-sm-6 mt-1">
            <div class="card">
              <div class="card-body">
                  <h5 class="card-title">Plano de contingência</h5>
                  <p class="card-text text-justify">Planejamento de riscos, ou plano de recuperação de desastres, tem o objetivo de descrever as medidas a serem tomadas por uma empresa.</p>
                  <a href="#" class="btn btn-primary">Visitar</a>
              </div>
            </div>
        </div>
    </div>
</div>

<div class="container mt-2">
    <div class="accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
            <h5 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                LGPD
                </button>
            </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <p class="text-justify">Pela lei que agora entra em vigor, o cidadão passa a ser titular de seus dados. Regras passam a ser impostas aos setores público e privado, que se tornam responsáveis pelo ciclo de um dado pessoal na organização: coleta, tratamento, armazenamento e exclusão. A lei vale tanto para meios online, como para os offline.</p>
              <p class="text-justify">Adequação completo à LGPD.</p>
            </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Visão Geral
                </button>
            </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
              <div class="card-body">
                  <p class="text-justify">Buscamos identificar os riscos por meio do modelo de análise criado e recomendado por intermédio do Sr. Ferreira e Araújo (2008, p. 179), Matriz do nível de risco e na LGPD.<br/>
        <p class="text-justify">A empresa se basea nas diretrizes propostas pela Norma ABNT NBR ISO/IEC 27002:2013.</p>
        <p class="text-justify">Palavras chaves: Informação, Política de Segurança da Informação.</p>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingThree">
              <h5 class="mb-0">
                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                 Conformidade
                  </button>
              </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
              <div class="card-body">
              <p class="text-justify">Precisa e confiável, não contém erros.</p>
              <p class="text-justify">Completa, abrange todos os fatos e aspectos importantes da empresa.</p>
              <p class="text-justify">Econômica, custa pouco para aplica-lá.</p>
              <p class="text-justify">Relevante de interesse do usuário.</p>
              <p class="text-justify">Simples, fácil entendimento.</p>
              <p class="text-justify">Pontual, disponível quando necessária.</p>
              <p class="text-justify">Verificável, pode-se conferir que está correta.</p>
              <p class="text-justify">Acessível, meio de acesso não dificultando o processo.</p>
              <p class="text-justify">Segura, apenas para usuarios autorizados.</p>

              </div>
            </div>
        </div>
              
        <div class="card">
            <div class="card-header" id="headingfour">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour" style="white-space: normal;">
                Tendo como referência a LGPD e a Norma ABNT NBR ISO/IEC 27002:2013
                </button>
            </h5>
            </div>
            <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample">
            <div class="card-body">
               <p class="text-justify">Oferecemos soluções em tecnologia e em recursos humanos, transformando e acompanhando pessoas e organizações para gerar vantagem competitiva a seus clientes.</p>
            </div>
            </div>
        </div>
    </div>
</div>

  <div class="container mt-2">
    <div class="row">
      <div class="col-sm-4">
        <h5 class="text-center">Ativos da informação</h5><br/>
        <p class="text-justify">A informação virou o principal item de valor para as empresas e é de fundamental importância que esta seja tratada de forma segura.</p>
        <p class="text-justify">Devido a esse crescente volume de informações que permeia as organizações, tanto em ambientes reais como em ambientes virtuais, proteger as informações dos mais variados tipos de ataque deve ser uma preocupação, não só dos profissionais da área específica, mas de todos da gestão coorporativa.</p>
        <p class="text-justify">A informação é um ativo avaliado como sensível de grande utilidade, portanto, deve ser adequadamente utilizada e protegida contra ameaças e riscos.</p>
      </div>
      <div class="col-sm-4 col-xs-4">
        <h5 class="text-center">Benefícios</h5><br/>
        <p class="text-justify">A empresa será beneficiada no uso correto dos sistemas, aplicações de TI e serviços de tecnologia.<br/><br/>
        Pela importância crescente de proteger as informações, apoiando-se no uso de software e valendo-se de recursos que garantam a efetiva segurança da informação.</p>
        <p class="text-justify">Garantia da preservação dos pilares da segurança da informação, "confidencialidade, integridade, disponibilidade, autenticidade e disponibilidade" que asseguram credibilidade e segurança da informação crítica na empresa.</p>
        
      </div>
      <div class="col-sm-4 col-xs-4">
        <h5 class="text-center">Objetivos Específicos </h5>
        <p class="text-justify">•	Visão geral de ativos.</p>
        <p class="text-justify">•	Classificação de ativos.</p>     
        <p class="text-justify">•	Matriz do nível de risco.</p>
        <p class="text-justify">•	Definição do nível de risco.</p>
        <p class="text-justify">•	Definição do nível de impacto.</p>      
        <p class="text-justify">•	Matriz de risco – Segurança física.</p>  
        <p class="text-justify">•	Matriz de risco – Segurança lógica.</p> 
        <p class="text-justify">•	Identificar os ativos de informação.</p>
        <p class="text-justify">•	Definição do nível de probabilidade.</p>        
        <p class="text-justify">•	Identificar vulnerabilidades por meio de análise de risco.</p> 
        <p class="text-justify">•	Desenvolver uma proposta de política de segurança da informação.</p>      
      </div>
    </div>
  </div>
</div>


  <div class="imagencontainer">
    <div class="container">
        <div class="embed-responsive embed-responsive-21by9">
        <!-- <iframe class="embed-responsive-item" src="..."></iframe> -->
        <iframe width="560" height="315" src="https://www.youtube.com/embed/6Af6b_wyiwI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
  </div>

<!-- Footer -->
<footer class="text-center text-lg-start bg-light text-muted">
  <!-- Section: Social media -->
  <section class="container justify-content-center justify-content-lg-between p-4 border-bottom" >
    <!-- Left -->
    
    <div class="me-5 d-none d-lg-block">
      <p class="text-justify"><span>Lei Geral de Proteção de Dados Pessoais (LGPD).: Lei nº 13.709/2018, é a legislação brasileira que regula as atividades de tratamento de dados pessoais e que também altera os artigos 7º e 16 do Marco Civil da Internet.</span></p>
    </div>
   
    <!-- Left -->

    <!-- Right -->
            <!-- <div>
            <a href="" class="me-4 text-reset ">
                <i class="fab fa-facebook-f">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-twitter">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-google">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-instagram">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-linkedin">teste</i>
            </a>
            <a href="" class="me-4 text-reset">
                <i class="fab fa-github">teste</i>
            </a>
            </div> -->
    <!-- Right -->
  </section>
  <!-- Section: Social media -->

  <!-- Section: Links  -->
  <section class="d-flex justify-content-center justify-content-lg-between p-2 border-bottom">
    <div class="container text-center text-md-start mt-1">
      <!-- Grid row -->
      <div class="row mt-1">
        <!-- Grid column -->
        <div class="col-sm-3 col-lg-4 col-xs-3 mx-auto mb-1">
          <!-- Content -->
          <h6 class="text-uppercase fw-bold mb-4">
            <i class="fas fa-gem me-3"></i>Segurança da Informação
          </h6>
          <p class="text-justify">
          A segurança da informação está diretamente relacionada com proteção de um conjunto de informações, no sentido de preservar o valor que possuem para um indivíduo ou uma organização. São propriedades básicas da segurança da informação: confidencialidade, integridade, disponibilidade, autenticidade e legalidade.
          </p>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Produtos
          </h6>        
          <p>
            <a href="#!" class="text-reset">Integridade </a>
          </p>   
          <p>
            <a href="#!" class="text-reset">Autenticidade </a>
          </p>   
          
          <p>
            <a href="#!" class="text-reset">Disponibilidade </a>
          </p>

          <p>
            <a href="#!" class="text-reset">Confidencialidade</a>
          </p>       
        </div>        

        <!-- Grid column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            links Úteis
          </h6>
          <p>
            <a href="#!" class="text-reset">Ajuda</a>
          </p>        
          <p>
            <a href="#!" class="text-reset">Normas</a>
          </p> 
          <p>
            <a href="#!" class="text-reset">Instruções</a>
          </p>
          <p>
            <a href="#!" class="text-reset">Configurações</a>
          </p>
        </div>
        
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          <!-- Links -->
          <h6 class="text-uppercase fw-bold mb-4">
            Contato
          </h6>
          <h6 class="text-left"><i class="fas fa-home me-3">Península dos Ministros<br/>St. de Habitações Individuais Sul <br/>QL 12 - Lago Sul, Brasília - DF<br/> 70385-010</i></h6>
          <p>
            <i class="fas fa-envelope me-3"></i>
            Wappixbr@gmail.com
          </p>
          <p><i class="fas fa-phone me-3"></i> + 55 61 9.9267-7435</p>
          <p><i class="fas fa-print me-3"></i> + 55 61 9.9999-9999</p>
        </div>
        <!-- Grid column -->
      </div>
      <!-- Grid row -->
    </div>
  </section>
  <!-- Section: Links  -->

  <!-- Copyright -->
  <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="https://www.google.com.br">Wappix.com</a>   <!--  https://mdbootstrap.com/  -->
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->
</div>

    <script src="public_html/js/jquery-3.3.1.slim.min.js"></script>
    <script src="public_html/js/popper.min.js"></script>
    <script src="public_html/js/bootstrap.min.js"></script>
    
  </body>
</html>



